#!/bin/bash
base=$(curl -s "http://bts.ai/api/v1/get_fill_order_history?limit=1&market=BRIDGE.SCP_BRIDGE.BTC"|jq .fill_orders[].fill_price.base.amount|sed 's/"//g')
quote=$(curl -s "http://bts.ai/api/v1/get_fill_order_history?limit=1&market=BRIDGE.SCP_BRIDGE.BTC"|jq .fill_orders[].fill_price.quote.amount|sed 's/"//g')
btc=$(curl -s http://api.coindesk.com/v1/bpi/currentprice.json |jq -r '.bpi[] | select (.code=="USD").rate'|sed 's/,//g' )

totalcoins=$(curl -sA "SiaPrime-Agent" "localhost:4280/explorer"|jq .totalcoins | tr -d '"'| rev | cut -c25- | rev)

base=$(echo $base*0.00000001|bc)
quote=$(echo $quote*0.0001|bc)
priceinbtc=$(printf %.10f $(echo $base / $quote|bc -l))
priceinsat=$(echo $priceinbtc*100000000|bc -l|sed 's/0\+$//;s/\.$/.0/')
priceusd=$(echo $priceinbtc*$btc| bc -l)
marketcap=$(echo $priceusd*$totalcoins| bc -l)

if [[ "$priceinsat" == .* ]]; then
priceinsat=0$priceinsat                                                            
fi                                                                                 
                                                                                   
echo "Price (SAT): $priceinsat"                                                    
echo "Price (USD): \$0$priceusd"                                                   
echo "Market Cap:  \$${marketcap%.*}"   

echo "$priceinsat sat" > /mnt/Discord-Bot/price.txt