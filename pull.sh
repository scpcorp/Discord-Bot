export NODE_PATH=/mnt/Discord-Bot/node_modules/

localfile="/mnt/Discord-Bot/index.js"
tempfile="/mnt/Discord-Bot/index.js.temp"
remotefile="https://gitlab.com/SiaPrime/Discord-Bot/raw/master/index.js"

wget -q $remotefile -O $tempfile

diff=$(diff $localfile $tempfile )

if [ "$diff" != "" ] 
then
echo $(date)
    echo "The file was modified, pulling new copy and restarting bot"
    mv -f $tempfile $localfile

    ps ax | grep Discord-Bot | grep -v "grep" | awk '{print $1}' | xargs kill -9 $1
    cd /mnt/Discord-Bot
    /usr/local/bin/node /mnt/Discord-Bot &
fi

if [ $tempfile ] 
then 
    rm -rf $tempfile
fi
